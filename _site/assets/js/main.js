$(document).ready(function () {

  // Toggle mobile menu open / closed
  $('.js-toggle-menu').click(function (e) {
    e.preventDefault();
    var t = $(this);
    var target = $(t.attr('href'));
    if (target.length) {
      if (target.hasClass('open')) {
        target.removeClass('open');
      } else {
        target.addClass('open');
      }
    }
  });


  // Toggle sub menu items if viewing mobile menu:
  if ($(window).width() < 900) {

    $('.js-toggle-sub-menu').click(function (e) {
      var t = $(this);
      var subMenu = t.parent().find('.main-menu__sub-menu');
      if (subMenu.length) {
        e.preventDefault();
        if (subMenu.hasClass('open')) {
          subMenu.removeClass('open');
        } else {
          $('.main-menu__sub-menu.open').removeClass('open');
          subMenu.addClass('open');
        }
      }
    });

  }


  /*
    Initialise image liquid plugin to convert content images to background
    images for easier responsive positioning.
  */
  $('.js-img-fill').imgLiquid();


  // Initialise home page carousel:
  var carouselEl = $('.js-carousel');
  if (carouselEl.length) {
    carouselEl.each(function () {
      var t = $(this);
      var carouselList = t.find('.carousel__list');

      var carousel = carouselList.bxSlider({
        mode: 'fade',
        auto: true,
        pager: false,
        controls: false,
        pause: 4000,
        speed: 800
      });

      t.find('.js-carousel-prev').on('click', function (e) {
        e.preventDefault();
        carousel.goToPrevSlide();
      });

      t.find('.js-carousel-next').on('click', function (e) {
        e.preventDefault();
        carousel.goToNextSlide();
      });
    });
  }


  // Initialise testimonials slider:
  var sliderEl = $('.js-testimonials-slider');
  if (sliderEl.length) {
    sliderEl.each(function () {
      var t = $(this);
      var slides = t.find('.testimonials-slider__item');
      var maxHeight = 0;

      // Allow lis to render then check for max height:
      setTimeout(function () {
        slides.each(function () {
          var slideHeight = $(this).height();
          if (slideHeight > maxHeight) maxHeight = slideHeight;
        });

        // Set all slides to same (max) height to avoid slider growing/shrinking
        slides.css({ height: maxHeight + 'px' });

        // Initialise slider:
        t.bxSlider({
          mode: 'fade',
          auto: true,
          pager: true,
          controls: false,
          pause: 5000,
          speed: 500,
          autoHover: true
        });
      }, 100);
    });
  }


  // Intialise product gallery slider:
  var productGalleryEl = $('.js-product-gallery');
  if (productGalleryEl.length) {
    productGalleryEl.each(function () {
      var t = $(this);

      // Initialise slider:
      var productGallery = t.bxSlider({
        mode: 'fade',
        auto: true,
        pager: false,
        controls: false,
        pause: 5000,
        speed: 500,
        autoHover: true
      });

      // Intitialise thumb images to replace large version when clicked
      $('.js-product-gallery-pager').on('click', function (e) {
        e.preventDefault();
        var index = $('.product__gallery-thumb').index($(this));
        productGallery.goToSlide(index);
      });
    });
  }


  // Intialise gallery slider:
  var galleryEl = $('.js-gallery-slider');
  if (galleryEl.length) {
    galleryEl.each(function () {
      var t = $(this);
      var galleryList = t.find('.product__gallery--gallery');
      // Initialise slider:
      var gallery = galleryList.bxSlider({
        mode: 'fade',
        auto: true,
        pager: false,
        controls: false,
        pause: 4000,
        speed: 500
      });

      t.find('.js-carousel-prev').on('click', function (e) {
        e.preventDefault();
        gallery.goToPrevSlide();
      });

      t.find('.js-carousel-next').on('click', function (e) {
        e.preventDefault();
        gallery.goToNextSlide();
      });
    });
  }


  // Click handler for pinboard settings mini-menu
  $('.js-mini-menu-trigger').on('click', function (e) {
    e.stopPropagation();
    var t = $(this);
    if (t.hasClass('open')) {
      t.removeClass('open');
    } else {
      t.addClass('open');
      $('body').on('click', closeMiniMenu);
    }
  });

  function closeMiniMenu (e) {
    e.stopPropagation();
    $('.js-mini-menu-trigger').removeClass('open');
    $('body').off('click', closeMiniMenu);
  }


  // Display related functionality for pin/un-pin buttons:
  $('.js-pin-btn').on('click', function (e) {
    e.preventDefault();
    var t = $(this);

    // If tile was already pinned, unpin it:
    if (t.hasClass('pin-btn--active')) {
      unPinTile(t);
    // Otherwise, add pin:
    } else {
      pinTile(t);
    }
  });

  function pinTile (pinBtnElement) {
    var parentTile = pinBtnElement.closest('.product-tile');
    pinBtnElement.addClass('pin-btn--active');
    parentTile.addClass('product-tile--active');
    pinBtnElement.find('.icon-pin').removeClass('icon-pin').addClass('icon-pin-solid');
  }

  function unPinTile (pinBtnElement) {
    var parentTile = pinBtnElement.closest('.product-tile');
    pinBtnElement.removeClass('pin-btn--active');
    parentTile.removeClass('product-tile--active');
    pinBtnElement.find('.icon-pin-solid').removeClass('icon-pin-solid').addClass('icon-pin');

    // Show warning to let user proceed or undo unpin tile action:
    showConfirmModal();
  }

  // Append modal to DOM
  function showConfirmModal () {
    $('body').append('<div class="modal"><div class="modal__box"><h3 class="modal__title">You have deleted your pinned item.</h3><a class="modal__btn btn btn--outline-reversed js-undo-delete-pin">Oh no - undo!</a><a class="modal__btn btn btn--outline js-delete-pin">Yes - delete forever</a></div></div>').addClass('js-modal-present');
  }

  // Remove modal from the DOM:
  function closeConfirmModal () {
    $('body').removeClass('js-modal-present');
    $('.modal').remove();
  }

  // Attach event listeners to modal buttons:
  $('body').on('click', '.js-delete-pin', function (e) {
    e.preventDefault();
    // TODO: Add functionality here to delete pin from pinboard
    closeConfirmModal();
  }).on('click', '.js-undo-delete-pin', function (e) {
    e.preventDefault();
    // TODO: Add functionality here to undo delete action
    closeConfirmModal();
  });


  // Initialise fancybox gallery for product images
  $('.js-gallery-modal-trigger').fancybox({
    padding: 0,
    afterLoad: function(current, previous) {
      // Append pin button to fancybox slide element:
      // TODO: need to set state of this to whether image is already pinned or not:
      current.inner.append('<span class="product__pin-btn pin-btn js-pin-btn js-fancybox-pin-btn"><i class="pin-btn__icon icon-pin"></i></span>');

      // Add click handler for pinning/unpinning current slide:
      $('.js-fancybox-pin-btn').on('click', function (e) {
        e.preventDefault();
        var t = $(this);

        // If tile was already pinned, unpin it:
        if (t.hasClass('pin-btn--active')) {
          unPinTile(t);
        // Otherwise, add pin:
        } else {
          pinTile(t);
        }
      });
    }
  });


  // Toggle comment form on blog page
  $('.js-toggle-comment-form').on('click', function (e) {
    e.preventDefault();
    var t = $(this);
    var targetForm = $(t.attr('href'));

    if (targetForm.length) {
      if (targetForm.hasClass('show')) {
        t.removeClass('active');
        targetForm.removeClass('show');
      } else {
        $('.js-toggle-comment-form').removeClass('active');
        $('.blog-article__form').removeClass('show');
        targetForm.addClass('show');
        t.addClass('active');
      }
    }
  });


  // Toggle active form on contact page
  $('.js-form-switch').on('click', function (e) {
    e.preventDefault();
    var t = $(this);
    var targetForm = $(t.attr('href'));

    if (targetForm.length) {
      if (!targetForm.hasClass('show')) {
        $('.js-form-switch').removeClass('active');
        $('.contact-page__form').removeClass('show');
        targetForm.addClass('show');
        t.addClass('active');
      }
    }
  });

});
